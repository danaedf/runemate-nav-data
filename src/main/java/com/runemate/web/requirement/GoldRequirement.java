package com.runemate.web.requirement;

import com.runemate.web.*;
import lombok.*;

@Value
public class GoldRequirement implements Requirement {

    int gold;

    @Override
    public boolean satisfy(@NonNull final WebContext context) {
        return context.hasGold(gold);
    }

    @Override
    public int type() {
        return Type.GOLD;
    }
}
