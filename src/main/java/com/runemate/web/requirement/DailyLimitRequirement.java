package com.runemate.web.requirement;

import com.runemate.web.*;
import lombok.*;

@Value
public class DailyLimitRequirement implements Requirement {

    DiaryRequirement.Region region;
    int limit;

    @Override
    public boolean satisfy(@NonNull WebContext context) {
        return limit == -1 || context.hasRemainingUses(region, limit);
    }

    @Override
    public int type() {
        return Type.DAILY_LIMIT;
    }
}
