package com.runemate.web.transport.fixed;

import lombok.*;
import lombok.experimental.*;

@Getter
@FieldDefaults(level = AccessLevel.PRIVATE, makeFinal = true)
@SuperBuilder(toBuilder = true)
public class FairyRingTransport extends FixedTransport {

    @NonNull String code;
    float cost;

}
