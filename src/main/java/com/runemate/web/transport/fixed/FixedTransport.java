package com.runemate.web.transport.fixed;

import com.runemate.web.model.*;
import com.runemate.web.requirement.*;
import lombok.*;
import lombok.experimental.*;

@Getter
@FieldDefaults(level = AccessLevel.PRIVATE, makeFinal = true)
@SuperBuilder(toBuilder = true)
public class FixedTransport {

    @NonNull Coordinate source;
    @NonNull Coordinate destination;
    @NonNull @Builder.Default Requirement requirement = Requirements.none();

}
